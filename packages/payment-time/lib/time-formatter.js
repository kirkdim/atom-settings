'use babel'

export default {


  formatSeconds (seconds) {
    if (seconds == null || +seconds !== seconds) return '--:--'

    const currency= atom.config.get('payment-time.currency') || "$";
    const price= atom.config.get('payment-time.price') || 15;
    const showTime = atom.config.get('payment-time.showTime');
    const round = atom.config.get('payment-time.round');
    const payment =  price / 3600 * seconds

    const min = Math.floor(seconds / 60)
    const sec = seconds - 60 * min
    const time = `${('00' + min).slice(-2)}:${('00' + sec).slice(-2)}`
    return (showTime ? time : "")  + " " + (round ? payment.toFixed(0) : payment.toFixed(2)) + " " + currency
  },


}

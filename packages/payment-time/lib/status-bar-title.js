'use babel'

import { EventEmitter } from 'events'
import { formatSeconds } from './time-formatter'

export default class StatusBarTile extends EventEmitter {
  constructor () {
    super()
    this.render()
  }

  render (seconds) {
    if (this.element == null) {
      // Hourglass icon
      this.iconSpan = document.createElement('span')
      this.iconSpan.className = 'icon icon-hourglass'

      // Time display
        this.timeSpan = document.createElement('span')
        this.timeSpan.addEventListener('click', () => this.emit('time_click'))
        this.timeSpan.addEventListener('mouseover', () => this.toggleHighlight(this.timeSpan, true))
        this.timeSpan.addEventListener('mouseout', () => this.toggleHighlight(this.timeSpan, false))

      // Container
      this.element = document.createElement('div')
      this.element.className = 'inline-block payment-tile'
      this.element.appendChild(this.iconSpan)
      this.element.appendChild(this.timeSpan)
    }

    // Set time
    this.timeSpan.textContent = formatSeconds(seconds)
  }

  toggleHighlight (element, hovered) {
    const highlightClass = 'text-warning'
    if (hovered) {
      if (!element.classList.contains(highlightClass)) element.classList.add(highlightClass)
    } else {
      element.classList.remove(highlightClass)
    }
  }
  
  getElement () { return this.element }

  destroy () { this.element.remove() }
}

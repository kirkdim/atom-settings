'use babel';
import path from 'path';
import { CompositeDisposable } from 'atom';
import PaymentTimeView from './payment-time-view';
import StatusBarTile from './status-bar-title';

export default {
  config: {
  currency: {
    title: 'Currency',
    description: 'The currency in which you are paid',
    type: 'string',
    default: '$',
    minimum: 1
    },
    price: {
      title: 'Rate per hour',
      description: '',
      type: 'integer',
      default: 15
    },
    showTime: {
      title: 'Enable show time',
      description: '',
      type: 'boolean',
      default: true
    },
    round: {
      title: 'Enable round',
      description: 'For example: 1.52$ -> 1$ ',
      type: 'boolean',
      default: false
    }
  },

  subscriptions: null,

  activate(state) {
    this.paymentTimeView = new PaymentTimeView(state.paymentTimeViewState);
    this.modalPanel = atom.workspace.addModalPanel({
      item: this.paymentTimeView.getElement(),
      visible: false
    });

    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();

    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'payment-time:start': () => this.start(),
      'payment-time:pause': () => this.pause(),
      'payment-time:reset': () => this.reset()
    }));



  },
  consumeStatusBar (statusBar) {
    this.statusBarTile = new StatusBarTile()
    this.statusBarTile.on('time_click', () => this.pause())

    statusBar.addRightTile({
      item: this.statusBarTile.getElement(),
      priority: 999
    })
  },

  deactivate() {
    this.subscriptions.dispose()

    for (let view of ['statusBarTile', 'statsView']) {
      if (this[view] != null) {
        this[view].destroy && this[view].destroy()
        this[view] = null
      }
    }
  },

  serialize() {
    return {
      paymentTimeViewState: this.paymentTimeView.serialize()
    };
  },

  start() {

    /*atom.notifications.addInfo('PaymentTime', {
      detail: "Started!",
      icon: 'hourglass'
    });*/

    if (this.seconds == null) {
      this.seconds = 0;
      this.isStoped = false;
      this.countUp();
    } else {
      this.isStoped = false;
      this.countUp();
    }
  },
  pause() {
    if (!this.isStoped) {
      this.isStoped = true;
    } else {
      this.isStoped = false;
      this.countUp();
    }
      //this.statusBarTile.render(this.seconds);
  },
  reset() {
    this.seconds = 0;
  },
  countUp () {
    if (!this.isStoped ) {
        this.seconds++;
        this.tick();
        setTimeout(() => this.countUp(), 1000);
      }
      return
    },
  tick () {
    this.statusBarTile.render(this.seconds)
  }

};

(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var MongoInternals = Package.mongo.MongoInternals;
var Mongo = Package.mongo.Mongo;
var _ = Package.underscore._;
var Router = Package['iron:router'].Router;
var RouteController = Package['iron:router'].RouteController;
var ReactiveVar = Package['reactive-var'].ReactiveVar;
var check = Package.check.check;
var Match = Package.check.Match;
var Roles = Package['alanning:roles'].Roles;
var Iron = Package['iron:core'].Iron;
var Collection2 = Package['aldeed:collection2-core'].Collection2;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;

/* Package-scope variables */
var Krypton, self;

(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                   //
// packages/krypton_core/core.js                                                                     //
//                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                     //
Krypton = Krypton || {};

Krypton.collections = {};

Krypton.Collection = function(name, options) {
  var c = new Mongo.Collection(name, options);

  Krypton.collections[name] = c;

  return c;
};

Krypton.getCollection = function(name) {
  return Krypton.collections[name];
};

///////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                   //
// packages/krypton_core/route.js                                                                    //
//                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                     //
// Collections admin Routes
Meteor.startup(function() {
  _.forEach(Krypton.collections, function(collection) {
    Router.route(`/admin/${collection._name}`, {
      action: function(){
        this.render('KryptonCollectionShow')
      },
      data: {
        label: collection.label,
        name: collection._name,
        collections: collection.find({}),
      }
    });

    Router.route(`/admin/${collection._name}/new`, {
      action: function(){
        this.render('KryptonCollectionNew')
      },
      data: {
        collection: collection.label
      }
    });

    Router.route(`/admin/${collection._name}/:_id/edit`, {
      action: function() {
        this.render('KryptonCollectionEdit')
      },
      data: function() {
        var temp = {};
        temp.doc = collection.findOne({_id: this.params._id});
        temp.collection = collection.label;
        return temp;
      }
    });
  });
});

// Account Routes
Router.route(`/user`, {
  action: function(){
    this.render('KryptonAccounts')
  },
});

///////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                   //
// packages/krypton_core/publication.js                                                              //
//                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                     //
Meteor.startup(function() {
  Meteor.publish("images", function() {
    return Images.find({});
  });

  _.forEach(Krypton.collections, function(collection) {
    Meteor.publish(`${collection._name}.admin`, function() {
      return collection.find({});
    });
  });


  _.forEach(Krypton.collections, function(collection) {
    Meteor.publish(`${collection._name}`, function(query = {}, options = {}) {
      self = this;
      var args = Array.prototype.slice.call(arguments);
      self.language = _.last(args);
      var cursor = Mongo.Collection.get(`${collection._name}`).find(query, options).observeChanges({
        added: function (id, doc) {
          if (self.language != 'en' && doc.Translation && doc.Translation[self.language]) {
            for (var prop in doc.Translation[self.language]) {
              doc[prop] = doc.Translation[self.language][prop];
            }
            delete doc.Translation;
          }
          else {
            delete doc.Translation;
          }

          self.added(`${collection._name}`, id, doc);
        },
        changed: function (id, fields) {
          self.changed(`${collection._name}`, id, fields);
        },
        removed: function (id) {
          self.removed(`${collection._name}`, id);
        }
      });

      self.ready();

      self.onStop(function () {
        if (cursor) {
          cursor.stop();
        }
      })
    });
  });
});

///////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
(function (pkg, symbols) {
  for (var s in symbols)
    (s in pkg) || (pkg[s] = symbols[s]);
})(Package['krypton:core'] = {}, {
  Krypton: Krypton
});

})();

FROM centos:centos7
MAINTAINER Kirk Deam <me@kirkdeam.com>

ARG DB_NAME
ARG DB_USER
ARG DB_PASSWORD
ARG DB_ROOT_PASSWORD

# Install various utilities
RUN yum -y install iproute python-setuptools sendmail curl wget unzip git nano hostname openssh-clients yum-utils which epel-release openssh-server gettext

# Configure SSH
RUN ssh-keygen -b 1024 -t rsa -f /etc/ssh/ssh_host_key \
&& ssh-keygen -b 1024 -t rsa -f /etc/ssh/ssh_host_rsa_key \
&& ssh-keygen -b 1024 -t dsa -f /etc/ssh/ssh_host_dsa_key \
&& sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config \
&& sed -ri 's/#UsePAM no/UsePAM no/g' /etc/ssh/sshd_config

# Set root password
RUN echo root:root | chpasswd && yum install -y passwd

# Install Python and Supervisor
RUN yum -y install python-setuptools \
&& mkdir -p /var/log/supervisor \
&& easy_install supervisor

# Install NGINX
RUN yum -y install nginx
COPY nginx.conf /etc/nginx/nginx.conf

# Install MariaDB
COPY MariaDB.repo /etc/yum.repos.d/MariaDB.repo
RUN yum clean all;yum -y install mariadb-server mariadb-client

# Install PHP 7.1
RUN wget http://rpms.remirepo.net/enterprise/remi-release-7.rpm
RUN rpm -Uvh remi-release-7.rpm
RUN yum install -y php71-php-bcmath php71-php-cli php71-php-common php71-php-fpm php71-php-gd php71-php-intl php71-php-json php71-php-mbstring php71-php-mcrypt php71-php-mysqlnd php71-php-opcache php71-php-pdo php71-php-pear php71-php-pecl-uploadprogress php71-php-pecl-zip php71-php-soap php71-php-xml php71-php-xmlrpc

# Install confd and add template files.
COPY confd /usr/bin/confd
RUN chmod +x /usr/bin/confd
COPY conf.d /etc/confd/conf.d
COPY templates /etc/confd/templates

#Install composer
COPY composer.phar /usr/sbin/composer
RUN chmod 755 /usr/sbin/composer

# Make php7.1 default php
RUN mv /usr/bin/php71 /usr/bin/php

# Make /var/www/html default bash directory
RUN echo "cd /var/www/html" >> /root/.bashrc

ENV PATH="/var/www/html/vendor/bin:${PATH}"

EXPOSE 80 443 3306

# Copy drupal_install.sh and drush to container
COPY drupal_install.sh /drupal_install.sh
RUN chmod +x /drupal_install.sh
RUN echo $DB_NAME
RUN /drupal_install.sh $DB_NAME $DB_USER $DB_PASSWORD $DB_ROOT_PASSWORD

# Copy run.sh to container
COPY run.sh /run.sh
RUN chmod +x /run.sh

ENTRYPOINT ["/run.sh"]

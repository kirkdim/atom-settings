var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var sftp = require('gulp-sftp')
var cleanCSS = require('gulp-clean-css');

var input = '/sass/*.scss';
var output = '/css';

var config = {
  server: '',
  remotePath: '',
  user: '',
  pass: ''
}

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded',
  includePaths: ['./node_modules/breakpoint-sass/stylesheets', './node_modules/bootstrap-sass/assets/stylesheets'],
};

gulp.task('styles', function() {
  gulp.src('sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css/'))
    .pipe(browserSync.stream({match: '**/*.css'}))
    // .pipe(sftp({
    //   host: config.server,
    //   user: config.user,
    //   // pass: config.pass,
    //   remotePath: config.remotePath
    // }))
    .resume();
});

// Watch task
gulp.task('watch', function() {
  gulp.watch('sass/**/*.scss',['styles'])
  .on('change', function(event) {
    console.log('Change detected. Compiling ...');
  });
});

gulp.task('minify', function() {
  return gulp.src('./css/**/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist'));
});

gulp.task('browser-sync', function() {
  browserSync.init({
    proxy: {{ getenv "NGINX_HOST" }}:8080
  });
});

gulp.task('default', ['browser-sync', 'styles', 'watch'])

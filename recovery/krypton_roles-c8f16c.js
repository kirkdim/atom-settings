(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var _ = Package.underscore._;
var Accounts = Package['accounts-base'].Accounts;
var Tracker = Package.tracker.Tracker;
var Deps = Package.tracker.Deps;
var MongoInternals = Package.mongo.MongoInternals;
var Mongo = Package.mongo.Mongo;
var check = Package.check.check;
var Match = Package.check.Match;
var Anonymous = Package['krypton:users'].Anonymous;

/* Package-scope variables */
var currentUser, isMongoMixError, strContains, Roles, Permissions;

(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/krypton_roles/roles.server.js                                                                          //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
"use strict"

/**
 * Roles collection documents consist only of an id and a role name.
 *   ex: { _id: "123", name: "admin" }
 */
if (!Meteor.roles) {
  Meteor.roles = new Mongo.Collection("roles")

  // Create default indexes for roles collection
  Meteor.roles._ensureIndex('name', {unique: 1})
}

/**
* Publish logged-in user's so client-side checks can work.
*
* Use a named publish function so clients can check `ready()` state.
*/
Meteor.publish('krypton_permissions', function () {
  var loggedInUserId = this.userId,
  currentuser = {},
  fields = {
    permissions: 1,
    name: 1,
  };

  if (loggedInUserId) {
    currentUser = Meteor.users.findOne(
      {_id: loggedInUserId}
    );
  }
  else {
    // Else is an anonymous user
    currentUser.roles = ['anonymous'];
  }
  console.log(currentUser.roles);
  return Meteor.roles.find({name: {$in: currentUser.roles}}, {fields: fields});
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/krypton_roles/helpers.common.js                                                                        //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
isMongoMixError = function isMongoMixError (errorMsg) {
  var expectedMessages = [
      'Cannot apply $addToSet modifier to non-array',
      'Cannot apply $addToSet to a non-array field',
      'Can only apply $pullAll to an array',
      'Cannot apply $pull/$pullAll modifier to non-array',
      "can't append to array using string field name",
      'to traverse the element'
      ]

  return _.some(expectedMessages, function (snippet) {
    return strContains(errorMsg, snippet)
  })
}

strContains = function strContains (haystack, needle) {
  return -1 !== haystack.indexOf(needle)
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/krypton_roles/roles.common.js                                                                          //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
;(function () {

  /**
   * Provides functions related to user authorization. Compatible with built-in Meteor accounts packages.
   *
   * @module Roles
   */

  /**
   * Roles collection documents consist only of an id, a role name.
   *   ex: { _id:<uuid>, name: "admin" }
   */
  if (!Meteor.roles) {
    Meteor.Roles = new Mongo.Collection("roles")
  }

  /**
   * Authorization package compatible with built-in Meteor accounts system.
   *
   * Stores user's current roles in a 'roles' field on the user object.
   *
   * @class Roles
   * @constructor
   */
  if ('undefined' === typeof Roles) {
    Roles = {}
  }

  "use strict";

  _.extend(Roles, {
    /**
     * Create a new role.
     *
     * @method createRole
     * @param {String} role Name of role
     * @return {String} id of new role
     */
    createRole: function (role) {
      var id,
          match

      if (!role
        || 'string' !== typeof role
        || role.trim().length === 0) {

        throw new Error("Role must be specified and must be a string")
        return
      }

      try {
        id = Meteor.roles.insert({'name': role.trim()})
        return id
      }
      catch (e) {
        // (from Meteor accounts-base package, insertUserDoc func)
        // XXX string parsing sucks, maybe
        // https://jira.mongodb.org/browse/SERVER-3069 will get fixed one day
        if (/E11000 duplicate key error.*(index.*roles|roles.*index).*name/.test(e.err || e.errmsg)) {
          throw new Error("Role '" + role.trim() + "' already exists.")
        }
        else {
          throw e
        }
      }
    },

    /**
     * @TODO Remove role from user object
     * Remove role from db and from user object.
     * @method removeRole
     * @param {String} role Name of role
     */
    removeRole: function (role) {
      if (!role) return

      Roles.removeRoleFromUsers(role);

      var thisRole = Meteor.roles.findOne({name: role})
      if (thisRole) {
        Meteor.roles.remove({_id: thisRole._id})
      }
    },

    /**
     * Returns role object from db
     * @method getRole
     * @param {String} role Name of role
     */
    getRole: function (role) {
      if (!role) return

      return Meteor.roles.findOne({name: role})
    },

    /**
     * Add roles to users.
     *
     * @example
     *     Roles.addRolesToUsers(userID, 'admin')
     *     Roles.addRolesToUsers([userID1, userID2], ['admin', 'editor'])
     *
     * @method addRolesToUsers
     * @param {Array|String} users User id(s) or object(s) with an _id field
     * @param {Array|String} roles Name(s) of roles to add users to
     */
    addRolesToUsers: function (users, roles) {
      // use Template pattern to update user roles
      Roles._updateUserRoles(users, roles, Roles._update_$addToSet_fn)
    },

    /**
     * Add anonymous role to anonymous user.
     *
     * @example
     *     Roles.addRolesToUsers(anonymousObject)
     *
     * @method addRolesToUsers
     * @param {Array|String} users User id(s) or object(s) with an _id field
     * @param {Array|String} roles Name(s) of roles to add users to
     */
    addRoleToAnonymous: function (anonymousUser) {
      // use Template pattern to update user roles
      Roles._updateUserRoles(anonymousUser, 'anonymous', Roles._update_$addToSet_fn, true);
    },

    /**
     * Set a users roles.
     *
     * @example
     *     Roles.setUserRoles(userID, 'admin')
     *     Roles.setUserRoles([userID1, userID2], ['admin', 'editor'])
     *
     * @method setUserRoles
     * @param {Array|String} users User id(s) or object(s) with an _id field
     * @param {Array|String} roles Name(s) of roles to add users to
     */
    setUserRoles: function (users, roles) {
      // use Template pattern to update user roles
      Roles._updateUserRoles(users, roles, Roles._update_$set_fn)
    },

    /**
     * Remove a role from all the users
     *
     * @example
     *     Roles.removeRoleFromUsers('admin')
     *
     * @method removeRoleFromUsers
     * @param {Array|String} roles name
     */
    removeRoleFromUsers: function (role) {
      Meteor.users.find({roles: {$in: [role]}}).forEach(function (user) {
      	var roles = user.roles;

        roles = roles.filter(item => item !== role)

        Roles.setUserRoles(user._id, roles)
      });
    },

    /**
     * Private function 'template' that uses $set to construct an update object
     * for MongoDB.  Passed to _updateUserRoles
     *
     * @method _update_$set_fn
     * @protected
     * @param {Array} roles
     * @return {Object} update object for use in MongoDB update command
     */
    _update_$set_fn: function  (roles) {
      var update = {}

      // roles is an array of strings
      update.$set = {roles: roles}

      return update
    },  // end _update_$set_fn

    /**
     * Private function 'template' that uses $addToSet to construct an update
     * object for MongoDB.  Passed to _updateUserRoles
     *
     * @method _update_$addToSet_fn
     * @protected
     * @param {Array} roles
     * @return {Object} update object for use in MongoDB update command
     */
    _update_$addToSet_fn: function (roles) {
      var update = {}

      // roles is an array of strings
      update.$addToSet = {roles: {$each: roles}}

      return update
    },  // end _update_$addToSet_fn

    /**
     * Internal function that uses the Template pattern to adds or sets roles
     * for users.
     *
     * @method _updateUserRoles
     * @protected
     * @param {Array|String} users User id(s) or object(s) with an _id field
     * @param {Array|String} roles Name(s) of roles to add users to
     * @param {Function} updateFactory Func which returns an update object that
     *                         will be passed to Mongo.
     * @param {Boolean} anonymous Whether the user(s) is anonymous or not.
     */
    _updateUserRoles: function (users, roles, updateFactory, anonymous = false) {
      if (!users) throw new Error ("Missing 'users' param")
      if (!roles) throw new Error ("Missing 'roles' param")

      var existingRoles,
          query,
          update

      // ensure arrays to simplify code
      if (!_.isArray(users)) users = [users]
      if (!_.isArray(roles)) roles = [roles]

      // remove invalid roles
      roles = _.reduce(roles, function (memo, role) {
        if (role
            && 'string' === typeof role
            && role.trim().length > 0) {
          memo.push(role.trim())
        }
        return memo
      }, [])

      // ensure all roles exist in 'roles' collection
      existingRoles = _.reduce(Meteor.roles.find({}).fetch(), function (memo, role) {
        memo[role.name] = true
        return memo
      }, {})
      _.each(roles, function (role) {
        if (!existingRoles[role]) {
          Roles.createRole(role)
        }
      })

      // ensure users is an array of user ids
      users = _.reduce(users, function (memo, user) {
        var id
        if ('string' === typeof user) {
          memo.push(user)
        }
        else if ('object' === typeof user) {
          id = user._id
          if ('string' === typeof id) {
            memo.push(id)
          }
        }
        return memo
      }, [])

      // update all roles
      update = updateFactory(roles)

      try {
        if (Meteor.isClient) {
          // On client, iterate over each user to fulfill Meteor's
          // 'one update per ID' policy
          _.each(users, function (user) {
            if (anonymous) {
              Krypton.getCollection('anonymous').update({_id: user}, update)
            }
            else {
              Meteor.users.update({_id: user}, update)
            }
          })
        } else {
          // On the server we can use MongoDB's $in operator for
          // better performance
          if (anonymous) {
            Krypton.getCollection('anonymous').update(
              { _id: {
                  $in: users
                }
              },
              update,
              { multi: true }
            );
          }
          else {
            Meteor.users.update(
              { _id: {
                  $in: users
                }
              },
              update,
              { multi: true }
            );
          }
        }
      }
      catch (ex) {
        if (ex.name === 'MongoError' && isMongoMixError(ex.err || ex.errmsg)) {
          throw new Error (mixingGroupAndNonGroupErrorMsg)
        }

        throw ex
      }
    },

    /**
     * Check if user has specified permissions
     *
     * @example
     *     // non-group usage
     *     Roles.userHasPermission('can access', userID)
     *     Roles.userHasPermission(['can access', 'can update'])
     *
     * @method userHasPermission
     * @param {String|Array} roles Name of permission(s)
     * @param {String|Object} optional user User Id or actual user object
     * @return {Boolean} true if current user has permission
     */
    userHasPermission: function (permissions) {
      var userRoles,
          userPermissions = [],
          user;

      // Get current user or guest user
      if (Meteor.KryptonUser()) {
        user = Meteor.KryptonUser();
      }

      // ensure array to simplify code
      if (!_.isArray(permissions)) {
        permissions = [permissions]
      }

      userRoles = user.roles

      // Iterate through user roles and extract the permissions
      _.each(userRoles, function(roleName) {
        var role = Roles.getRole(roleName);
        _.each(role.permissions, function(permission) {
          if (permission && !_.contains(userPermissions, permission)) {
            userPermissions.push(permission);
          }
        });
      });


      // Check if user has the permissions we want.
      return _.some(permissions, function (permission) {
        return _.contains(userPermissions, permission)
      })
    },
  });
}());

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/krypton_roles/permissions.common.js                                                                    //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
;(function () {
  /**
   * Provides functions related to user authorization. Compatible with built-in Meteor accounts packages.
   *
   * @module Permissions
   */

  /**
   * Permissions collection documents consist only of an id and a permission name.
   *   ex: { _id:<uuid>, name: "can update"}
   */
  if (!Meteor.permissions) {
    Meteor.permissions = new Mongo.Collection("permissions")
  }

  /**
   * Authorization package compatible with built-in Meteor accounts system.
   *
   * Stores role's current permissions in a 'permissions' field on the roles object.
   *
   * @class Permissions
   * @constructor
   */
  if ('undefined' === typeof Permissions) {
    Permissions = {}
  }

  "use strict";

  var mixingGroupAndNonGroupErrorMsg = "Permissions error: Can't mix grouped and non-grouped roles for same user";

  _.extend(Permissions, {
    /**
     * Create a new permission. Whitespace will be trimmed.
     *
     * @method createPermission
     * @param {String} permission Name of permission
     * @return {String} id of new permission
     */
    createPermission: function (permission) {
      var id,
          match

      if (!permission ||
        'string' !== typeof role ||
        role.trim().length === 0) {
        return
      }

      try {
        id = Meteor.permissions.insert({'name': permission})
        return id
      }
      catch (e) {
        // (from Meteor accounts-base package, insertUserDoc func)
        // XXX string parsing sucks, maybe
        // https://jira.mongodb.org/browse/SERVER-3069 will get fixed one day
        if (/E11000 duplicate key error.*(index.*permissions|permissions.*index).*name/.test(e.err || e.errmsg)) {
          throw new Error("Permission '" + permission + "' already exists.")
        }
        else {
          throw e
        }
      }
    },

    /**
     * @TODO Remove permission from role object
     * Delete permission from db and from role object.
     * @method removePermission
     * @param {String} permission Name of permission
     */
    removePermission: function (permission) {
      if (!permission) return

      var foundRoles = Meteor.roles.find({permissions: {$in: [permission]}})

      if (foundRoles) {
        _.each(foundRoles, function(role) {
          // @TODO Remove permission from role.
          //
        });
      }

      return;

      var thisPermission = Meteor.permissions.findOne({name: permission})
      if (thisPermission) {
        Meteor.permission.remove({_id: thisPermission._id})
      }
    },

    /**
     * Add permissions to roles.
     *
     * Makes 2 calls to database:
     *  1. retrieve list of all existing roles
     *  2. update roles' permissions
     *
     * @example
     *     Permissions.addUsersToPermissions('can access', 'admin')
     *     Permissions.addUsersToPermissions(['can access', 'can update'], ['admin', 'editor'])
     *
     * @method addPermissionsToRoles
     * @param {Array|String} permissions Name(s) of permissions
     * @param {Array|String} roles Role id(s) or object(s) with an name field
     */
    addPermissionsToRoles: function (permissions, roles) {
      // use Template pattern to update user roles
      Permissions._updateRolePermissions(permissions, roles, Permissions._update_$addToSet_fn)
    },

    /**
     * Set a users roles/permissions.
     *
     * @example
     *     Permissions.setRolePermissions('can access', 'admin')
     *     Permissions.setRolePermissions(['can access', 'can update'], ['admin', 'editor'])
     *
     * @method setRolePermissions
     * @param {Array|String} roles Role name(s) or object(s) with an name field
     * @param {Array|String} permissions Name(s) of permissions
     */
    setRolePermissions: function (permissions, roles) {
      // use Template pattern to update user permissions
      Permissions._updateRolePermissions(permissions, roles, Permissions._update_$set_fn)
    },

    /**
     * Remove a permission from all roles
     *
     * @example
     *     Roles.removeRoleFromUsers('has access')
     *
     * @method removePermissionFromRoles
     * @param {Array|String} permission's name
     */
    removePermissionFromRoles: function (permission) {
      Meteor.roles.find({permissions: {$in: [permission]}}).forEach(function (role) {
        var permissions = role.permissions;

        permissions = permissions.filter(item => item !== permission)

        Permissions.addPermissionsToRoles(role._id, permissions)
      });
    },

    /**
     * Retrieve set of all existing permissions
     *
     * @method getAllPermissions
     * @return {Cursor} cursor of existing permissions
     */
    getAllPermissions: function () {
      return Meteor.permissions.find({}, {sort: {name: 1}})
    },

    /**
     * Private function 'template' that uses $set to construct an update object
     * for MongoDB.  Passed to _updateUserPermissions
     *
     * @method _update_$set_fn
     * @protected
     * @param {Array} permissions
     * @return {Object} update object for use in MongoDB update command
     */
    _update_$set_fn: function  (permissions) {
      var update = {}

      // permissions is an array of strings
      update.$set = {permissions: permissions}

      return update
    },  // end _update_$set_fn

    /**
     * Private function 'template' that uses $addToSet to construct an update
     * object for MongoDB.  Passed to _updateUserPermissions
     *
     * @method _update_$addToSet_fn
     * @protected
     * @param {Array} permissions
     * @return {Object} update object for use in MongoDB update command
     */
    _update_$addToSet_fn: function (permissions) {
      var update = {}

      // permissions is an array of strings
      update.$addToSet = {permissions: {$each: permissions}}

      return update
    },  // end _update_$addToSet_fn


    /**
     * Internal function that uses the Template pattern to adds or sets roles
     * for users.
     *
     * @method _updateRolePermissions
     * @protected
     * @param {Array|String} permissions Name(s) of permissions
     * @param {Array|String} roles Role name(s) or object(s) with an name field
     * @param {Function} updateFactory Func which returns an update object that
     *                         will be passed to Mongo.
     *   @param {Array} roles
     */
    _updateRolePermissions: function (permissions, roles, updateFactory) {
      if (!permissions) throw new Error ("Missing 'permissions' param")
      if (!roles) throw new Error ("Missing 'roles' param")

      var existingPermissions,
          query,
          update

      // ensure arrays to simplify code
      if (!_.isArray(permissions)) permissions = [permissions]
      if (!_.isArray(roles)) roles = [roles]

      // remove invalid permissions
      permissions = _.reduce(permissions, function (memo, permission) {
        if (permission
            && 'string' === typeof permission
            && permission.trim().length > 0) {
          memo.push(permission.trim())
        }
        return memo
      }, [])

      // empty permissions array is ok, since it might be a $set operation to clear roles
      //if (roles.length === 0) return

      // ensure all permissions exist in 'permissions' collection
      existingPermissions = _.reduce(Meteor.permissions.find({}).fetch(), function (memo, permission) {
        memo[permission.name] = true
        return memo
      }, {})
      _.each(permissions, function (permission) {
        if (!existingPermissions[permission]) {
          Permissions.createPermission(permission)
        }
      })

      // ensure roles is an array of role names
      roles = _.reduce(roles, function (memo, role) {
        var name
        if ('string' === typeof role) {
          memo.push(role)
        } else if ('object' === typeof role) {
          name = role.name
          if ('string' === typeof name) {
            memo.push(name)
          }
        }
        return memo
      }, [])

      // update all roles @TODO "????? What is that ?????"
      update = updateFactory(permissions)

      try {
        if (Meteor.isClient) {
          // On client, iterate over each user to fulfill Meteor's
          // 'one update per ID' policy
          _.each(roles, function (role) {
            Meteor.roles.update({name: role}, update)
          })
        } else {
          // On the server we can use MongoDB's $in operator for
          // better performance
          Meteor.roles.update(
            {name: {$in: roles}},
            update,
            {multi: true})
        }
      }
      catch (ex) {
        if (ex.name === 'MongoError' && isMongoMixError(ex.err || ex.errmsg)) {
          throw new Error (mixingGroupAndNonGroupErrorMsg)
        }

        throw ex
      }
    }  // end _updateUserPermissions

  })  // end _.extend(getPermissionsForRoles ...)

}());

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// packages/krypton_roles/playground.js                                                                            //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
Meteor.startup(function (){
  // Roles.addRolesToUsers('co96XhYQxFF6bnnfr', ['admin', 'editor'])
  Permissions.setRolePermissions('has access', 'anonymous');
  // Roles.removeRole('editor');
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
(function (pkg, symbols) {
  for (var s in symbols)
    (s in pkg) || (pkg[s] = symbols[s]);
})(Package['krypton:roles'] = {}, {
  Roles: Roles,
  Permissions: Permissions
});

})();
